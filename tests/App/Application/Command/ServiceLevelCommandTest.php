<?php

namespace App\Application\Command;

use App\Application\InputInterface;
use App\Application\OutputInterface;
use App\Domain\ServiceLevelIndicators\Code;
use App\Domain\ServiceLevelIndicators\Latency;
use App\Domain\ServiceLevelIndicators\SLIFactory;
use App\Domain\ServiceLevelObjectives\Config;
use App\Domain\ServiceLevelObjectives\ExceedMetric;
use App\Domain\Service;
use App\Infrastructure\CLI\Input;
use App\Infrastructure\CLI\Output;
use App\Infrastructure\Provider\Log\ItemProvider;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class ServiceLevelCommandTest extends TestCase
{
    private const LOG_LINE = '192.168.32.181 - - [14/06/2017:16:45:01 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 500 2 23.510983 "-" "@list-item-updater" prio:0';

    private ServiceLevelCommand $command;

    /**
     * @var SLIFactory|MockObject
     */
    private $SLIFactory;
    /**
     * @var Service|MockObject
     */
    private $service;
    /**
     * @var ItemProvider|MockObject
     */
    private $provider;
    private InputInterface $input;
    private OutputInterface $output;

    protected function setUp(): void
    {
        $this->SLIFactory = $this->createMock(SLIFactory::class);
        $this->service = $this->createMock(Service::class);
        $this->input = $this->createMock(Input::class);
        $this->output = $this->createMock(Output::class);
        $this->provider = $this->createMock(ItemProvider::class);
        $this->command = new ServiceLevelCommand(
            $this->SLIFactory,
            $this->service,
            $this->provider,
        );
    }

    public function testExecute(): void
    {
        $code = '5**';
        $codeSli = new Code($code);
        $this->SLIFactory->expects(self::once())
            ->method('getCode')
            ->with($code)
            ->willReturn($codeSli);

        $latency = '15';
        $latencySli = new Latency($latency);
        $this->SLIFactory->expects(self::once())
            ->method('getLatency')
            ->with($latency)
            ->willReturn($latencySli);

        $percentile = 99;
        $interval = 1;
        $this->input
            ->method('getArgument')
            ->withConsecutive(['i'], ['t'], ['u'])
            ->willReturnOnConsecutiveCalls($interval, $latency, $percentile);
        $config = new Config(
            $percentile,
            \DateInterval::createFromDateString('1 second'),
            [$codeSli, $latencySli]
        );

        $this->service->expects(self::once())
            ->method('setConfig')
            ->with($config);

        $this->input->expects(self::once())
            ->method('getStdIn')
            ->willReturn($this->getIterator([self::LOG_LINE]));
        $this->provider->expects(self::once())
            ->method('setItemProvider')
            ->with($this->getIterator([]));

        $metricAsString = 'testString';
        $exceedMetric = $this->createMock(ExceedMetric::class);
        $exceedMetric->expects(self::once())
            ->method('toString')
            ->willReturn($metricAsString);

        $this->service->expects(self::once())
            ->method('getExceedingMetrics')
            ->with($this->provider)
            ->willReturn($this->getIterator([$exceedMetric]));

        $this->output->expects(self::once())
            ->method('writeLn')
            ->with($metricAsString);

        $this->command->execute($this->input, $this->output);
    }

    private function getIterator(array $data): \Iterator
    {
        foreach ($data as $datum) {
            yield $datum;
        }
    }
}
