<?php

namespace App\Application;

use App\Application\Command\CommandInterface;
use App\Application\Command\ServiceLevelCommand;
use App\Infrastructure\CLI\Input;
use App\Infrastructure\CLI\Output;
use PHPUnit\Framework\TestCase;

class AppKernelTest extends TestCase
{
    private InputInterface $input;
    private OutputInterface $output;
    private CommandInterface $command;

    private AppKernel $kernel;

    protected function setUp(): void
    {
        $this->input = $this->createMock(Input::class);
        $this->output = $this->createMock(Output::class);
        $this->command = $this->createMock(ServiceLevelCommand::class);

        $this->kernel = new AppKernel();
        $this->kernel
            ->setCommand($this->command)
            ->setInput($this->input)
            ->setOutput($this->output)
        ;
    }

    public function testRun(): void
    {
        $this->command->expects(self::once())
            ->method('execute')
            ->with($this->input, $this->output);

        $this->kernel->run();
    }
}
