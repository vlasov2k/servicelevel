<?php

namespace App\Domain\ServiceLevelIndicators;

use App\Infrastructure\Provider\Log\AccessLogLine;
use PHPUnit\Framework\TestCase;

class LatencyTest extends TestCase
{
    public function testIsExceedReturnTrue(): void
    {
        $latency = new Latency(15);
        $dateTime = new \DateTime();
        $httpStatus = 500;
        $httpRequestLatency = 10;

        $item = new AccessLogLine($dateTime, $httpStatus, $httpRequestLatency);

        $this->assertTrue($latency->isItemSuccessful($item));
    }

    public function testIsExceedReturnFalse(): void
    {
        $latency = new Latency(15);
        $dateTime = new \DateTime();
        $httpStatus = 500;
        $httpRequestLatency = 20;

        $item = new AccessLogLine($dateTime, $httpStatus, $httpRequestLatency);

        $this->assertFalse($latency->isItemSuccessful($item));
    }
}
