<?php

namespace App\Domain\ServiceLevelIndicators;

use PHPUnit\Framework\TestCase;

class SLIFactoryTest extends TestCase
{
    private SLIFactory $factory;

    protected function setUp(): void
    {
        $this->factory = new SLIFactory();
    }

    public function testGetErrorCodePattern(): void
    {
        $code = $this->factory->getCode('500');

        $this->assertInstanceOf(Code::class, $code);
    }

    public function testGetLatency(): void
    {
        $code = $this->factory->getLatency('45');

        $this->assertInstanceOf(Latency::class, $code);
    }

    public function testGetCodeException(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        $this->factory->getCode(2);
    }

    public function testGetLatencyException(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        $this->factory->getLatency(-2);
    }
}
