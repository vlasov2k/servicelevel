<?php

namespace App\Domain\ServiceLevelIndicators;

use App\Domain\Log\ItemInterface;
use PHPUnit\Framework\TestCase;

class CodeTest extends TestCase
{
    public function testIsItemSuccessfulReturnTrue(): void
    {
        $code = new Code('500');
        $item = $this->createMock(ItemInterface::class);
        $item->expects($this->once())
            ->method('getCode')
            ->willReturn(200);

        $result = $code->isItemSuccessful($item);

        $this->assertEquals(true, $result);
    }

    public function testIsItemSuccessfulReturnFalse(): void
    {
        $code = new Code('5**');
        $item = $this->createMock(ItemInterface::class);
        $item->expects($this->once())
            ->method('getCode')
            ->willReturn(500);

        $result = $code->isItemSuccessful($item);

        $this->assertEquals(false, $result);
    }
}
