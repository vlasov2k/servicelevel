<?php

namespace App\Domain\ServiceLevelObjectives;

use PHPUnit\Framework\TestCase;

class ConfigTest extends TestCase
{
    public function testConstructorInvalidIntervalException(): void
    {
        $percentile = 99;
        $interval = \DateInterval::createFromDateString('0 seconds');
        $SLIs = [];

        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Interval parameter ( -i ) not valid');
        new Config($percentile, $interval, $SLIs);
    }

    public function testInvalidPercentileException(): void
    {
        $percentile = 101;
        $interval = \DateInterval::createFromDateString('1 seconds');
        $SLIs = [];

        $this->expectException(\InvalidArgumentException::class);
        $this->expectExceptionMessage('Percentile parameter ( -u ) not valid');
        new Config($percentile, $interval, $SLIs);
    }
}
