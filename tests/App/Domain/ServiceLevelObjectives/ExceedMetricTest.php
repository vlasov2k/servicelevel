<?php

namespace App\Domain\ServiceLevelObjectives;

use App\Domain\Log\ItemInterface;
use PHPUnit\Framework\TestCase;

class ExceedMetricTest extends TestCase
{
    private const PERCENTILE = 99;
    private const DATE_FORMAT = 'H:i:s';
    private const METRIC_AS_STRING_PATTERN = '%s %s %d';

    private ExceedMetric $metric;
    private ItemInterface $item;
    private string $resultTimeString;

    protected function setUp(): void
    {
        $controlTime = new \DateTime();
        $this->item = $this->createMock(ItemInterface::class);
        $this->item->method('getTime')
            ->willReturn($controlTime);

        $this->metric = new ExceedMetric((float)self::PERCENTILE, $this->item->getTime());

        $this->resultTimeString = $controlTime->format(self::DATE_FORMAT);
    }

    public function testHaveExceedsReturnTrue(): void
    {
        $this->metric->addErrorItem($this->item);
        $this->metric->addErrorItem($this->item);
        $this->metric->addSuccessItem($this->item);

        $this->assertTrue($this->metric->haveExceeds());
    }

    public function testHaveExceedsReturnFalse(): void
    {
        $this->metric->addSuccessItem($this->item);
        $this->metric->addSuccessItem($this->item);
        $this->metric->addSuccessItem($this->item);

        $this->assertFalse($this->metric->haveExceeds());
    }

    public function testToString(): void
    {
        $expectedSuccessPercentile = 33;
        $expectedResult = sprintf(
            self::METRIC_AS_STRING_PATTERN,
            $this->resultTimeString,
            $this->resultTimeString,
            $expectedSuccessPercentile
        );

        $this->metric->addErrorItem($this->item);
        $this->metric->addErrorItem($this->item);
        $this->metric->addSuccessItem($this->item);

        $metricAsString = $this->metric->toString();

        $this->assertEquals($expectedResult, $metricAsString);
    }

    public function testAddSuccessItem(): void
    {

        $expectedSuccessPercentile = 100;
        $expectedResult = sprintf(
            self::METRIC_AS_STRING_PATTERN,
            $this->resultTimeString,
            $this->resultTimeString,
            $expectedSuccessPercentile
        );

        $this->metric->addSuccessItem($this->item);
        $currentResult = $this->metric->toString();

        $this->assertEquals(
            $expectedResult, $currentResult
        );
    }

    public function testAddErrorItem(): void
    {

        $expectedSuccessPercentile = 0;
        $expectedResult = sprintf(
            self::METRIC_AS_STRING_PATTERN,
            $this->resultTimeString,
            $this->resultTimeString,
            $expectedSuccessPercentile
        );

        $this->metric->addErrorItem($this->item);
        $currentResult = $this->metric->toString();

        $this->assertEquals(
            $expectedResult, $currentResult
        );
    }
}
