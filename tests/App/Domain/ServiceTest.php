<?php

namespace App\Domain;

use App\Domain\Log\ItemCollectionInterface;
use App\Domain\ServiceLevelIndicators\Code;
use App\Domain\ServiceLevelIndicators\Latency;
use App\Domain\ServiceLevelObjectives\Config;
use App\Infrastructure\Provider\Log\AccessLogLine;
use PHPUnit\Framework\TestCase;

class ServiceTest extends TestCase
{
    private const ERROR_CODE = 500;
    private const TIME_INTERVAL = '10 seconds';
    private const PERCENTILE = 99;
    private const LATENCY = 30;

    private array $accessLog = [
        '192.168.32.181 - - [14/06/2017:16:45:00 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:01 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:02 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 50.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:03 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 500 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:04 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:05 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:06 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 500 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:07 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:07 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:08 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:09 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:11 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:12 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:13 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:14 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:15 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:16 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:17 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:18 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:19 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:20 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:21 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:22 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 50.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:23 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:24 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:25 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 500 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:26 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 50.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:27 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 50.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:28 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:28 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:29 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:30 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:31 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 50.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:32 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 50.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:33 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 50.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:34 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 50.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:35 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 500 2 50.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:36 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 500 2 50.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:37 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 500 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:38 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 500 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:38 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 500 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:39 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:40 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:41 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:42 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:43 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:44 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:45 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:46 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:47 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:48 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:49 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:50 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:51 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:52 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:53 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 200 2 20.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:54 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 500 2 50.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:55 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 500 2 50.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:55 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 500 2 50.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:56 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 500 2 50.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:57 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 500 2 50.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:58 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 500 2 50.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:59 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 500 2 50.000000 "-" "@list-item-updater" prio:0',
        '192.168.32.181 - - [14/06/2017:16:45:60 +1000] "PUT /rest/v1.4/documents?zone=default&_rid=6076537c HTTP/1.1" 500 2 50.000000 "-" "@list-item-updater" prio:0',
    ];

    private array $expectedResults = [
        '16:45:00 16:45:09 72',
        '16:45:21 16:45:30 63',
        '16:45:31 16:45:40 18',
        '16:45:51 16:46:00 27',
    ];

    public function testGetExceedingMetrics(): void
    {
        $SLIs = [
            new Code(self::ERROR_CODE),
            new Latency(self::LATENCY)
        ];

        $config = new Config(
            (float)self::PERCENTILE,
            \DateInterval::createFromDateString(self::TIME_INTERVAL),
            $SLIs,
        );

        $service = new Service();
        $service->setConfig($config);

        $itemCollection = [];
        foreach ($this->accessLog as $row) {
            $itemCollection[] = AccessLogLine::createFromString($row);
        }

        $logProvider = $this->createMock(ItemCollectionInterface::class);
        $logProvider->expects(self::atLeastOnce())
            ->method('getNext')
            ->willReturn($this->getIterator($itemCollection));

        $metrics = $service->getExceedingMetrics($logProvider);

        $index = 0;
        foreach($metrics as $metric) {
            $metricAsString = $metric->toString();
            $this->assertEquals($this->expectedResults[$index], $metricAsString);

            $index++;
        }

        $expectedMetricsCount = 4;
        $this->assertEquals($expectedMetricsCount, $index);
    }

    private function getIterator(array $data): \Iterator
    {
        foreach ($data as $datum) {
            yield $datum;
        }
    }
}
