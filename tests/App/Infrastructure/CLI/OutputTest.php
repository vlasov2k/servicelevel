<?php

namespace App\Infrastructure\CLI;

use PHPUnit\Framework\TestCase;

class OutputTest extends TestCase
{
    public function testWriteLn(): void
    {
        $string = 'test';
        $this->expectOutputString($string . PHP_EOL);

        $output = new Output();
        $output->writeLn($string);
    }

}
