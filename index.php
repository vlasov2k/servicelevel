#!/usr/bin/env php
<?php

declare(strict_types = 1);

ini_set('memory_limit', '16M');

require __DIR__ . '/vendor/autoload.php';

use App\Application\AppKernel;
use App\Application\DependencyProvider;

$provider = new DependencyProvider();
$command = $provider->getCommand();
$input = $provider->getInput();
$output = $provider->getOutput();

$kernel = new AppKernel();

$kernel->setCommand($command)
    ->setInput($input)
    ->setOutput($output)
    ->run();
