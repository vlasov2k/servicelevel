<?php

declare(strict_types = 1);

namespace App\Application;

interface InputInterface
{
    /**
     * @return mixed
     */
    public function getArgument(string $name);

    public function getStdIn(): \Iterator;
}
