<?php

declare(strict_types = 1);

namespace App\Application\Command;

use App\Application\InputInterface;
use App\Application\OutputInterface;

interface CommandInterface
{
    public function execute(InputInterface $input, OutputInterface $output);
}
