<?php

declare(strict_types = 1);

namespace App\Application\Command;

use App\Infrastructure\Provider\Log\ItemProvider;
use App\Domain\ServiceLevelObjectives\Config;
use App\Domain\ServiceLevelObjectives\ExceedMetric;
use App\Domain\Service;
use App\Domain\ServiceLevelIndicators\SLIFactory;

class ServiceLevelCommand implements CommandInterface
{
    private const ERROR_CODE_PATTERN = '5**';
    private const TIME_INTERVAL = '10 seconds';

    private SLIFactory $sliFactory;
    private Service $slaService;
    private ItemProvider $itemProvider;

    public function __construct(
        SLIFactory $sliFactory,
        Service $slaService,
        ItemProvider $itemProvider
    ) {
        $this->sliFactory = $sliFactory;
        $this->slaService = $slaService;
        $this->itemProvider = $itemProvider;
    }

    public function execute($input, $output): void
    {
        try {
            $metricInterval = sprintf('%s seconds', $input->getArgument('i'));
        } catch (\InvalidArgumentException $exception) {
            $metricInterval = self::TIME_INTERVAL;
        }

        try {
            $SLIs = [
                $this->sliFactory->getCode(self::ERROR_CODE_PATTERN),
                $this->sliFactory->getLatency($input->getArgument('t'))
            ];

            $config = new Config(
                (float)$input->getArgument('u'),
                \DateInterval::createFromDateString($metricInterval),
                $SLIs,
            );
            $this->slaService->setConfig($config);

            $this->itemProvider->setItemProvider($input->getStdIn());

            $exceedMetrics = $this->slaService->getExceedingMetrics($this->itemProvider);
            foreach ($exceedMetrics as $exceedMetric) {
                /** @var ExceedMetric $exceedMetric */
                $output->writeLn($exceedMetric->toString());
            }
        } catch (\LogicException $exception) {
            $output->writeLn($exception->getMessage());
        }
    }
}
