<?php

declare(strict_types = 1);

namespace App\Application;

use App\Application\Command\CommandInterface;

class AppKernel
{
    private CommandInterface $command;
    private InputInterface $input;
    private OutputInterface $output;

    public function setCommand(CommandInterface $command): self
    {
        $this->command = $command;
        return $this;
    }

    public function setInput(InputInterface $input): self
    {
        $this->input = $input;
        return $this;
    }

    public function setOutput(OutputInterface $output): self
    {
        $this->output = $output;
        return $this;
    }

    public function run(): void
    {
        $this->command->execute($this->input, $this->output);
    }
}
