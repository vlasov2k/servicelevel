<?php

declare(strict_types = 1);

namespace App\Application;

interface OutputInterface
{
    public function writeLn($content): void;
}
