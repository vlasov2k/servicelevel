<?php

declare(strict_types=1);

namespace App\Application;

use App\Application\Command\CommandInterface;
use App\Application\Command\ServiceLevelCommand;
use App\Domain\ServiceLevelIndicators\SLIFactory;
use App\Domain\Service;
use App\Infrastructure\CLI\Input;
use App\Infrastructure\CLI\Output;
use App\Infrastructure\Provider\Log\ItemProvider;

class DependencyProvider
{
    public function getCommand(): CommandInterface
    {
        return new ServiceLevelCommand(
            new SLIFactory(),
            new Service(),
            new ItemProvider()
        );
    }

    public function getInput(): InputInterface
    {
        return new Input();
    }

    public function getOutput(): OutputInterface
    {
        return new Output();
    }
}
