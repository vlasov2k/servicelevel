<?php

declare(strict_types = 1);

namespace App\Domain\ServiceLevelObjectives;

use App\Domain\ServiceLevelIndicators\ServiceLevelIndicatorInterface;

class Config
{
    private float $percentile;
    private \DateInterval $interval;
    /**
     * @var ServiceLevelIndicatorInterface[]
     */
    private array $SLIs;

    /**
     * @param float $percentile
     * @param \DateInterval $interval
     * @param ServiceLevelIndicatorInterface[] $SLIs
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(float $percentile, \DateInterval $interval, array $SLIs)
    {
        if (100 < $percentile || $percentile < 0) {
            throw new \InvalidArgumentException('Percentile parameter ( -u ) not valid');
        }
        if ($interval->s === 0) {
            throw new \InvalidArgumentException('Interval parameter ( -i ) not valid');
        }
        $this->percentile = $percentile;
        $this->interval = $interval;
        $this->SLIs = $SLIs;
    }

    public function getInterval(): \DateInterval
    {
        return $this->interval;
    }

    /**
     * @return ServiceLevelIndicatorInterface[]
     */
    public function getSLIs(): array
    {
        return $this->SLIs;
    }

    public function getPercentile(): float
    {
        return $this->percentile;
    }
}
