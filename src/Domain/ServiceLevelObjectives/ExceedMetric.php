<?php

declare(strict_types = 1);

namespace App\Domain\ServiceLevelObjectives;

use App\Domain\Log\ItemInterface;

class ExceedMetric
{
    private const DATE_FORMAT = 'H:i:s';

    private float $percentile;
    private int $successCount = 0;
    private int $errorCount = 0;
    private \DateTimeInterface $startTime;
    private \DateTimeInterface $endTime;

    public function __construct(
        float $percentile,
        \DateTimeInterface $startTime
    ) {
        $this->percentile = $percentile;
        $this->startTime = $startTime;
    }

    public function haveExceeds(): bool
    {
        $errorBudget = 100 - $this->percentile;
        $errorPercents = $this->getErrorsPercent();

        return $errorPercents > $errorBudget;
    }

    public function addSuccessItem(ItemInterface $item): void
    {
        $this->endTime = $item->getTime();
        $this->successCount++;
    }

    public function addErrorItem(ItemInterface $item): void
    {
        $this->endTime = $item->getTime();
        $this->errorCount++;
    }

    public function getStartTime(): \DateTimeInterface
    {
        return $this->startTime;
    }

    public function toString(): string
    {
        return \sprintf(
            '%s %s %d',
            $this->startTime->format(self::DATE_FORMAT),
            $this->endTime->format(self::DATE_FORMAT),
            $this->getSuccessPercent(),
        );
    }

    private function getErrorsPercent(): float
    {
        $summaryCount = $this->successCount + $this->errorCount;
        if (!$summaryCount) {
            return 0;
        }

        $errorsPercent = ($this->errorCount / $summaryCount) * 100;

        return round($errorsPercent, 2);
    }

    private function getSuccessPercent(): float
    {
        $summaryCount = $this->successCount + $this->errorCount;
        $successPercent = ($this->successCount / $summaryCount) * 100;

        return round($successPercent, 2);
    }
}
