<?php

declare(strict_types = 1);

namespace App\Domain;

use App\Domain\Log\ItemInterface;
use App\Domain\Log\ItemCollectionInterface;
use App\Domain\ServiceLevelObjectives\Config;
use App\Domain\ServiceLevelObjectives\ExceedMetric;

class Service
{
    private Config $config;

    public function setConfig(Config $config): void
    {
        $this->config = $config;
    }

    public function getExceedingMetrics(ItemCollectionInterface $logLinesCollection): \Iterator
    {
        $metric = null;

        /** @var ItemInterface $item */
        foreach ($logLinesCollection->getNext() as $item) {
            if (
                isset($metric)
                && $this->isNextCheckingInterval(
                    $metric->getStartTime(),
                    $item->getTime()
                )
            ) {
                if ($metric->haveExceeds()) {
                    yield $metric;
                }
                unset($metric);
            }

            if (!isset($metric)) {
                $metric = new ExceedMetric(
                    $this->config->getPercentile(),
                    $item->getTime()
                );
            }

            if ($this->isExceedSLIs($item)) {
                $metric->addErrorItem($item);
            } else {
                $metric->addSuccessItem($item);
            }
        }

        if (
            isset($metric)
            && $metric->haveExceeds()
        ) {
            yield $metric;
        }
    }

    private function isExceedSLIs(ItemInterface $item): bool
    {
        foreach ($this->config->getSLIs() as $SLI) {
            if (!$SLI->isItemSuccessful($item)) {
                return true;
            }
        }

        return false;
    }

    private function isNextCheckingInterval(
        \DateTimeInterface $firstItemTime,
        \DateTimeInterface $currentItemTime
    ): bool {
        $time = clone $currentItemTime;
        $compareTime = $time->sub($this->config->getInterval());

        return $compareTime >= $firstItemTime;
    }
}
