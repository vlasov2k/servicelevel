<?php

declare(strict_types = 1);

namespace App\Domain\ServiceLevelIndicators;

use App\Domain\Log\ItemInterface;

class Code implements ServiceLevelIndicatorInterface
{
    private string $errorCodePattern;

    /**
     * @param string $errorCodePattern
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(string $errorCodePattern)
    {
        if (strlen($errorCodePattern) !== 3) {
            throw new \InvalidArgumentException(
                'Http status parameter not valid' . PHP_EOL
            );
        }
        $this->errorCodePattern = $errorCodePattern;
    }

    public function isItemSuccessful(ItemInterface $item): bool
    {
        $char = trim($this->errorCodePattern, '*');
        $itemCode = (string)$item->getCode();
        $compareResult = strpos($itemCode, $char);

        return $compareResult !== 0;
    }
}
