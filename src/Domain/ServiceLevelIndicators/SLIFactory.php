<?php

declare(strict_types=1);

namespace App\Domain\ServiceLevelIndicators;

class SLIFactory
{
    public function getLatency(string $latency): ServiceLevelIndicatorInterface
    {
        return new Latency((int)$latency);
    }

    public function getCode(string $code): ServiceLevelIndicatorInterface
    {
        return new Code($code);
    }
}
