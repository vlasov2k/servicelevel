<?php

declare(strict_types = 1);

namespace App\Domain\ServiceLevelIndicators;

use App\Domain\Log\ItemInterface;

interface ServiceLevelIndicatorInterface
{
    public function isItemSuccessful(ItemInterface $item): bool;
}
