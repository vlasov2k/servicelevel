<?php

declare(strict_types = 1);

namespace App\Domain\ServiceLevelIndicators;

use App\Domain\Log\ItemInterface;

class Latency implements ServiceLevelIndicatorInterface
{
    private int $latency;

    /**
     * @param int $latency
     *
     * @throws \InvalidArgumentException
     */
    public function __construct(int $latency)
    {
        if ($latency < 0) {
            throw new \InvalidArgumentException(
                'Latency ( -t ) parameter not valid' . PHP_EOL
            );
        }

        $this->latency = $latency;
    }

    public function isItemSuccessful(ItemInterface $item): bool
    {
        $itemLatency = $item->getLatency();
        return $this->latency > $itemLatency;
    }
}
