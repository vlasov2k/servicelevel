<?php

declare(strict_types = 1);

namespace App\Domain\Log;

interface ItemInterface
{
    public function getTime(): \DateTimeInterface;
    public function getCode(): int;
    public function getLatency(): int;
}
