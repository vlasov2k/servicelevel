<?php

declare(strict_types = 1);

namespace App\Domain\Log;

interface ItemCollectionInterface
{
    public function getNext(): \Iterator;
}
