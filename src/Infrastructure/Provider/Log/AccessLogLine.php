<?php

declare(strict_types = 1);

namespace App\Infrastructure\Provider\Log;

use App\Domain\Log\ItemInterface;

class AccessLogLine implements ItemInterface
{
    private const LINE_PATTERN = '/\[(.*)\]\s".*"\s(\d{3}).*\s(\d+\.\d+)/m';
    private const DATETIME_FORMAT = 'd/m/Y:H:i:s O';

    private \DateTimeInterface $dateTime;
    private int $httpStatus;
    private float $httpRequestLatency;

    /**
     * @param \DateTimeInterface $dateTime
     * @param int $httpStatus
     * @param float $httpRequestLatency
     */
    public function __construct(
        \DateTimeInterface $dateTime,
        int $httpStatus,
        float $httpRequestLatency
    ) {
        $this->dateTime = $dateTime;
        $this->httpStatus = $httpStatus;
        $this->httpRequestLatency = $httpRequestLatency;
    }

    public static function createFromString(
        string $line
    ): ItemInterface {
        preg_match(self::LINE_PATTERN,$line, $matches);

        return new self(
            \DateTime::createFromFormat(self::DATETIME_FORMAT, $matches[1]),
            (int)$matches[2],
            (float)$matches[3]
        );
    }

    public function getTime(): \DateTimeInterface
    {
        return $this->dateTime;
    }

    public function getCode(): int
    {
        return (int)$this->httpStatus;
    }

    public function getLatency(): int
    {
        return (int)$this->httpRequestLatency;
    }
}
