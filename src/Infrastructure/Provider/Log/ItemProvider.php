<?php

declare(strict_types = 1);

namespace App\Infrastructure\Provider\Log;

use App\Domain\Log\ItemCollectionInterface;

class ItemProvider implements ItemCollectionInterface
{
    private \Iterator $itemProvider;

    public function getNext(): \Iterator
    {
        foreach ($this->itemProvider as $item) {
            yield AccessLogLine::createFromString($item);
        }
    }

    public function setItemProvider(\Iterator $itemProvider): void
    {
        $this->itemProvider = $itemProvider;
    }
}
