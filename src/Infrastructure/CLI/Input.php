<?php

declare(strict_types = 1);

namespace App\Infrastructure\CLI;

use App\Application\InputInterface;

class Input implements InputInterface
{
    private const resourcePath = 'php://stdin';
    private const resourceAccessMode = 'rb';

    private array $arguments;

    public function __construct()
    {
        $this->parseArguments();
    }

    private function parseArguments(): void
    {
        $argvCount = count($_SERVER['argv']);
        for ($index = 1; $index < $argvCount; $index++) {
            if ($index % 2) {
                continue;
            }

            $this->arguments[$_SERVER['argv'][$index - 1]] = $_SERVER['argv'][$index];
        }
    }

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function getArgument(string $name)
    {
        if (!isset($this->arguments) || !array_key_exists("-{$name}", $this->arguments)) {
            throw new \InvalidArgumentException(sprintf('Parameter ( %s ) not valid', $name));
        }
        return $this->arguments["-{$name}"];
    }

    public function getStdIn(): \Iterator
    {
        $resource = \fopen(self::resourcePath, self::resourceAccessMode);

        while ($row = \fgets($resource)) {
            yield $row;
        }

        \fclose($resource);
    }
}
