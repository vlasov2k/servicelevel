<?php

declare(strict_types = 1);

namespace App\Infrastructure\CLI;

use App\Application\OutputInterface;

class Output implements OutputInterface
{
    public function writeLn($content): void
    {
        echo $content . PHP_EOL;
    }
}
