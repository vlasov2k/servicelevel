##Install

$```composer install```

##Run

$```cat /path/to/access.log | php index.php -u 99 -t 30 [-i 10]```

####Params:
```-u``` - success requests percentile<br />
```-t``` - success request latency limit (milliseconds)<br />
```-i``` - analyzed time interval (seconds), default - 10 seconds<br />
